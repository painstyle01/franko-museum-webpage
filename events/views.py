from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Event
from .serializers import EventSerializer

class EventView(APIView):
    def get(self, request):
        events = Event.objects.all()
        serializer = EventSerializer(events, many=True)
        return Response({"events": serializer.data})

    def post(self, request):
        event = request.data.get('event')

        # Create an article from the above data
        serializer = EventSerializer(data=event)
        if serializer.is_valid(raise_exception=True):
            event_saved = serializer.save()
        return Response({"success": "'{}' created successfully".format(event_saved.title)})
