from django.db import models

# Create your models here.
class Event(models.Model):
    title = models.TextField(max_length=120)
    time = models.DateTimeField()
    description = models.TextField()
    picture = models.URLField()