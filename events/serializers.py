from rest_framework import serializers
from .models import Event
class EventSerializer(serializers.Serializer):
    title = serializers.CharField()
    description = serializers.CharField()
    time = serializers.DateTimeField()
    picture = serializers.URLField()

    def create(self, validated_data):
        return Event.objects.create(**validated_data)