from django.urls import path

from .views import EventView


app_name = "articles"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('articles/', EventView.as_view()),
]
